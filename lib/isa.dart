library isa;

import 'package:measures/measures.dart';

import 'src/calculations.dart';
import 'src/constants.dart';

/// The params of the [InternationalStandardAtmosphere] have been calculated assuming
/// the air to be a perfect gas free from moisture and dust and based on
/// conventional initial values of [Temperature], [Pressure] and [Density] of the air
/// for mean sea level.
class InternationalStandardAtmosphere {
  final Altitude baseAlt;
  final Temperature baseTemp;
  final Pressure basePressure;
  final Density baseDensity;

  /// Creates an instance of [InternationalStandardAtmosphere] with default values
  /// or customized one from specific [Altitude], [Temperature], [Pressure] and [Density] values if provided.
  const InternationalStandardAtmosphere(
      {Altitude? baseAltitude,
      Temperature? baseTemperature,
      Pressure? basePressure,
      Density? baseDensity})
      : this.baseAlt = baseAltitude ?? const Altitude.fromMetres(h0),
        this.baseTemp = baseTemperature ?? const Temperature.fromKelvin(T0),
        this.basePressure = basePressure ?? const Pressure.fromHpa(P0),
        this.baseDensity = baseDensity ?? const Density.fromKgMCubic(p0);

  /// Creates an instance of [InternationalStandardAtmosphere] with default values of
  /// [Temperature], [Pressure] and [Density] and specific value of [Altitude].
  factory InternationalStandardAtmosphere.fromAltitude(Altitude baseAltitude) =>
      InternationalStandardAtmosphere(baseAltitude: baseAltitude);

  /// Creates an instance of [InternationalStandardAtmosphere] with default values of
  /// [Altitude], [Pressure] and [Density] and specific value of [Temperature].
  factory InternationalStandardAtmosphere.fromTemperature(
          Temperature baseTemperature) =>
      InternationalStandardAtmosphere(baseTemperature: baseTemperature);

  /// Creates an instance of [InternationalStandardAtmosphere] with default values of
  /// [Altitude], [Temperature] and [Density] and specific value of [Pressure].
  factory InternationalStandardAtmosphere.fromPressure(Pressure basePressure) =>
      InternationalStandardAtmosphere(basePressure: basePressure);

  /// Creates an instance of [InternationalStandardAtmosphere] with default values of
  /// [Altitude], [Temperature] and [Pressure] and specific value of [Density].
  factory InternationalStandardAtmosphere.fromDensity(Density baseDensity) =>
      InternationalStandardAtmosphere(baseDensity: baseDensity);

  /// Creates an instance of [InternationalStandardAtmosphere] with default values of
  /// [Altitude], [Pressure] and [Density] and variation of [Temperature] from standard.
  factory InternationalStandardAtmosphere.fromTemperatureVariation(
          num variationFromISA) =>
      InternationalStandardAtmosphere(
          baseTemperature: Temperature.fromKelvin(T0 + variationFromISA));

  /// Returns the geopotential [Altitude] for given geometrical [Altitude]
  Altitude getGeopotentialAt(final Altitude altitude) =>
      calcGeopotentialAlt(altitude);

  /// Returns the [Temperature] for given geometrical [Altitude]
  Temperature getTemperatureAt(final Altitude altitude) =>
      calcTemperature(altitude, baseTemperature: baseTemp);

  /// Returns the atmospheric [Pressure] for given geometrical [Altitude]
  Pressure getPressureAt(final Altitude altitude) => calcPressure(altitude,
      baseTemperature: baseTemp,
      basePressure: basePressure,
      baseAltitude: baseAlt);

  /// Returns the atmospheric [Density] for given geometrical [Altitude]
  Density getDensityAt(final Altitude altitude) =>
      calcDensity(altitude, baseTemperature: baseTemp);

  /// Returns the gravitational [Acceleration] for given geometrical [Altitude]
  Acceleration getGravityAccelAt(final Altitude altitude) => calcG(altitude);

  /// Returns the [Speed] of sound for given geometrical [Altitude]
  Speed getSoundSpdAt(final Altitude altitude) =>
      calcSoundSpdForAlt(altitude, baseTemperature: baseTemp);
}
