import 'dart:math';

import 'package:measures/measures.dart';

import 'constants.dart';

Acceleration calcG(final Altitude altitude) =>
    Acceleration.fromMsSq(g0 * pow(avgRadius / (avgRadius + altitude.m), 2));

Speed calcSoundSpdForTemp(final Temperature temperature) =>
    Speed.fromMs(sqrt(k * R * temperature.kelvin));

Speed calcSoundSpdForAlt(final Altitude altitude,
        {final Temperature? baseTemperature, final bool? isGeopotential}) =>
    calcSoundSpdForTemp(calcTemperature(altitude,
        baseTemperature: baseTemperature, isGeopotential: isGeopotential));

Density calcDensity(final Altitude altitude,
    {final Temperature? baseTemperature, final bool? isGeopotential}) {
  final T = calcTemperature(altitude,
          baseTemperature: baseTemperature, isGeopotential: isGeopotential)
      .kelvin;
  final p = calcPressure(altitude).hPa * 100.0;
  return Density.fromKgMCubic(p / (R * T));
}

double calcSpecificWeight(final Altitude altitude,
    {final Temperature? baseTemperature, final bool? isGeopotential}) {
  return calcDensity(altitude,
              baseTemperature: baseTemperature, isGeopotential: isGeopotential)
          .kgMCub *
      calcG(altitude).msSq;
}

Altitude calcPressureScaleHeight(final Altitude altitude,
    {final bool? isGeopotential}) {
  final T = calcTemperature(altitude, isGeopotential: isGeopotential).kelvin;
  final g = calcG(altitude).msSq;
  return Altitude.fromMetres(R * T / g);
}

Pressure calcPressure(final Altitude altitude,
    {final Pressure? basePressure,
    final Altitude? baseAltitude,
    final Temperature? baseTemperature}) {
  final basePress = basePressure ?? Pressure.fromHpa(1013.25);
  final baseAlt = baseAltitude ?? Altitude.fromMetres(0.0);
  final baseTemp = baseTemperature ?? Temperature.fromCelsius(15.0);
  final gradient = calcTempGradient(altitude);
  final isNotNullGradient = gradient.abs() > 0.00001;
  final geopotentialCurAlt = calcGeopotentialAlt(altitude).m;
  final roundedCurAlt = geopotentialCurAlt.roundToDouble();
  var geopotentialBaseAlt = Altitude.fromMetres(double.nan);
  if (roundedCurAlt <= 11000.0) {
    geopotentialBaseAlt = calcGeopotentialAlt(baseAlt);
  } else if (roundedCurAlt <= 20000.0) {
    geopotentialBaseAlt = Altitude.fromMetres(11000.0);
  } else if (roundedCurAlt <= 32000.0) {
    geopotentialBaseAlt = Altitude.fromMetres(20000.0);
  } else if (roundedCurAlt <= 47000.0) {
    geopotentialBaseAlt = Altitude.fromMetres(32000.0);
  } else if (roundedCurAlt <= 51000.0) {
    geopotentialBaseAlt = Altitude.fromMetres(47000.0);
  } else if (roundedCurAlt <= 71000.0) {
    geopotentialBaseAlt = Altitude.fromMetres(51000.0);
  } else if (roundedCurAlt <= 80000.0) {
    geopotentialBaseAlt = Altitude.fromMetres(71000.0);
  } else {
    geopotentialBaseAlt = Altitude.fromMetres(80000.0);
  }
  final pressure = roundedCurAlt <= 11000.0
      ? basePress.hPa
      : calcPressure(calcGeometricAlt(geopotentialBaseAlt),
              basePressure: basePress,
              baseAltitude: baseAlt,
              baseTemperature: baseTemp)
          .hPa;
  final temperature = calcTemperature(
          isNotNullGradient ? geopotentialBaseAlt : altitude,
          baseTemperature: baseTemp,
          isGeopotential: true)
      .kelvin;
  final deltaHeight = geopotentialCurAlt - geopotentialBaseAlt.m;
  double result;
  if (isNotNullGradient) {
    result = pressure *
        pow(1.0 + gradient / temperature * deltaHeight, -g0 / (gradient * R));
  } else {
    result = pressure * exp(-g0 / (R * temperature) * deltaHeight);
  }
  return Pressure.fromHpa(result);
}

double calcTempGradient(final Altitude altitude) {
  var gradient = 0.0;
  final geopotentialAltitude = calcGeopotentialAlt(altitude).m.roundToDouble();
  if (geopotentialAltitude.roundToDouble() <= 11000.0) {
    gradient = -0.0065;
  } else if (geopotentialAltitude <= 20000.0) {
    gradient = gradient;
  } else if (geopotentialAltitude <= 32000.0) {
    gradient = 0.001;
  } else if (geopotentialAltitude <= 47000.0) {
    gradient = 0.0028;
  } else if (geopotentialAltitude <= 51000.0) {
    gradient = gradient;
  } else if (geopotentialAltitude <= 71000.0) {
    gradient = -0.0028;
  } else if (geopotentialAltitude <= 80000.0) {
    gradient = -0.002;
  } else {
    gradient = gradient;
  }
  return gradient;
}

Temperature calcTemperature(final Altitude altitude,
    {final Temperature? baseTemperature, final bool? isGeopotential}) {
  final baseT = baseTemperature ?? Temperature.fromCelsius(15.0);
  double result;
  final curAlt =
      isGeopotential ?? false ? altitude.m : calcGeopotentialAlt(altitude).m;
  final roundedCurAlt = curAlt.roundToDouble();
  final gradient = calcTempGradient(altitude);
  if (roundedCurAlt <= 11000.0) {
    result = curAlt * gradient + baseT.celsius;
  } else if (roundedCurAlt <= 20000.0) {
    result = (curAlt - 11000.0) * gradient +
        calcTemperature(Altitude.fromMetres(11000.0),
                baseTemperature: baseT, isGeopotential: true)
            .celsius;
  } else if (roundedCurAlt <= 32000.0) {
    result = (curAlt - 20000.0) * gradient +
        calcTemperature(Altitude.fromMetres(20000.0),
                baseTemperature: baseT, isGeopotential: true)
            .celsius;
  } else if (roundedCurAlt <= 47000.0) {
    result = (curAlt - 32000.0) * gradient +
        calcTemperature(Altitude.fromMetres(32000.0),
                baseTemperature: baseT, isGeopotential: true)
            .celsius;
  } else if (roundedCurAlt <= 51000.0) {
    result = (curAlt - 47000.0) * gradient +
        calcTemperature(Altitude.fromMetres(47000.0),
                baseTemperature: baseT, isGeopotential: true)
            .celsius;
  } else if (roundedCurAlt <= 71000.0) {
    result = (curAlt - 51000.0) * gradient +
        calcTemperature(Altitude.fromMetres(51000.0),
                baseTemperature: baseT, isGeopotential: true)
            .celsius;
  } else if (roundedCurAlt <= 80000.0) {
    result = (curAlt - 71000.0) * gradient +
        calcTemperature(Altitude.fromMetres(71000.0),
                baseTemperature: baseT, isGeopotential: true)
            .celsius;
  } else {
    result = -76.5;
  }
  return Temperature.fromCelsius(result);
}

Altitude calcGeopotentialAlt(final Altitude geometricAltitude) =>
    geometricAltitude * avgRadius / (avgRadius + geometricAltitude.m);

Altitude calcGeometricAlt(final Altitude geopotentialAltitude) =>
    geopotentialAltitude * avgRadius / (avgRadius - geopotentialAltitude.m);
