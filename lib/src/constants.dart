/// The nominal earth's radius in metres
const avgRadius = 6356766.0;

/// Sea level mean molar mass in kg per kmol, as obtained from the perfect gas law
const M0 = 28.96442;

/// Universal gas constant in J/(K * kmol)
const Ru = 8314.32;

/// Specific gas constant in J/(K * kg)
const R = Ru / M0;

/// Standard acceleration due to gravity in metres per second squared.
/// It conforms with latitude ф = 45°32'33" using Lambert's equation of the
/// acceleration due to gravity as a function of latitude ф
const g0 = 9.80665;

/// Sutherland's empirical constant in the equation for dynamic viscosity
const betaS = 1.458e-6;

/// Sutherland's empirical constant in the equation for dynamic viscosity
const S = 110.4; // Kelvin degrees

/// Adiabatic index, the ratio of the specific heat of air at constant pressure
/// to its specific heat at constant volume
const k = 1.4;

/// Sea level temperature in Kelvin degrees
const T0 = 288.15;

/// Sea level altitude in metres
const h0 = 0.0;

/// Sea level atmospheric pressure in hectopascals
const P0 = 1013.25;

/// Sea level atmospheric density in kilograms per metre cubic
const p0 = 1.225;
