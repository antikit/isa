# isa

[![Pub Version](https://img.shields.io/pub/v/isa?logo=dart&logoColor=white)](https://pub.dev/packages/isa)
![Dart Platform](https://badgen.net/pub/dart-platform/isa)
![Flutter Platform](https://badgen.net/pub/flutter-platform/isa)
![SDK](https://badgen.net/pub/sdk-version/isa)
![License](https://badgen.net/pub/license/isa)
[![Pub Likes](https://badgen.net/pub/likes/isa)](https://pub.dev/packages/isa/score)
[![Pub points](https://badgen.net/pub/points/isa)](https://pub.dev/packages/isa/score)
[![Pub popularity](https://badgen.net/pub/popularity/isa)](https://pub.dev/packages/isa/score)

International Standard Atmosphere package. The package allows to find physical characteristics and
params of standard or customized atmosphere at the given altitude (up to 80 km) according to ICAO Doc 7488.

## About

This international standard specifies the characteristics of an ICAO standard atmosphere.
It is intended for use in calculations in the design of aircraft, in presenting test results of
aircraft and their components under identical conditions, and to facilitate standardization
in the development and calibration of instruments.
Its use is also recommended in the processing of data from geophysical and meteorological observations.

## Currently supported features

At the moment calculation of following physical characteristics is supported:
- Geometric and geopotential altitude;
- Temperature;
- Pressure;
- Density;
- Acceleration due to gravity;
- Speed of sound;
- and more to add.

## Overview

This package is intended to facilitate the uniform application of the ICAO standard atmosphere defined in
Annex 8 and to provide users of the standard atmosphere with convenient sets of data that are accurate enough for
practical applications, and that are based on internationally agreed physical constants and conversion factors.
It is worth noting that ICAO and ISO Standard Atmospheres are identical.

## Usage

### Basic use case

#### Use corresponding constructor to define standard or customized atmosphere model, and then get specific physical characteristics you need for given altitude. For example:

```dart
import 'package:isa/isa.dart';
import 'package:measures/measures.dart';

typedef ISA = InternationalStandardAtmosphere;

main() {
  final isa = ISA(); // Set ISA with standard values of temperature and pressure at 0.0 metres Above Mean Sea Level (AMSL)
  final curAltitude = Altitude.fromMetres(10000); // Set the altitude you seek params for
  final pressure = isa.getPressureAt(curAltitude); // Get the value of atmospheric pressure at 10000 metres AMSL
  final temperature = isa.getTemperatureAt(curAltitude); // Get the value of temperature at 10000 metres AMSL
  final soundSpeed = isa.getSoundSpdAt(curAltitude); // Get the value of speed of sound at 10000 metres AMSL
  print(pressure.hPa); // 264.9987
  print(temperature.celsius); // -49.8979
  print(soundSpeed.ms); // 299.53166
}
```
## Installation

Add isa to your `pubspec.yaml` file:

```yaml
dependencies:
  isa: ^1.0.0
```

## Changelog

All notable changes to this project will be documented in [this file](./CHANGELOG.md).

## Issues

For issues, file directly in the [isa repo](https://bitbucket.org/antikit/isa).

## License

[The 3-Clause BSD License](https://opensource.org/licenses/BSD-3-Clause)
