// Copyright (c) 2021, Anton Antonchik.  All rights reserved. Use of this source
// code is governed by a BSD-style license that can be found in the LICENSE file.

import 'package:isa/isa.dart';
import 'package:measures/measures.dart';

typedef ISA = InternationalStandardAtmosphere;

main() {
  final isa =
      ISA(); // Set ISA with standard values of temperature and pressure at 0.0 metres Above Mean Sea Level (AMSL)
  final curAltitude =
      Altitude.fromMetres(10000); // Set the altitude you seek params for
  final pressure = isa.getPressureAt(
      curAltitude); // Get the value of atmospheric pressure at 10000 metres AMSL
  final temperature = isa.getTemperatureAt(
      curAltitude); // Get the value of temperature at 10000 metres AMSL
  final soundSpeed = isa.getSoundSpdAt(
      curAltitude); // Get the value of speed of sound at 10000 metres AMSL
  print(pressure.hPa); // 264.9987
  print(temperature.celsius); // -49.8979
  print(soundSpeed.ms); // 299.53166
}
