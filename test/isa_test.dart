import 'package:isa/isa.dart';
import 'package:measures/measures.dart';
import 'package:test/test.dart';

typedef ISA = InternationalStandardAtmosphere;

void main() {
  final isa = ISA();
  test('Standard Atmosphere at Sea Level', () {
    final geometricAlt = Altitude.zero;
    expect(isa.getGeopotentialAt(geometricAlt).m, 0.0);
    expect(isa.getTemperatureAt(geometricAlt).kelvin, 288.15);
    expect(isa.getTemperatureAt(geometricAlt).celsius, 15.0);
    expect(isa.getPressureAt(geometricAlt).hPa, 1013.25);
    expect(isa.getDensityAt(geometricAlt).kgMCub, 1.225000001753089);
    expect(isa.getGravityAccelAt(geometricAlt).msSq, 9.80665);
    expect(isa.getSoundSpdAt(geometricAlt).ms, 340.2939902999749);
  });
  test('Standard Atmosphere at 3000 m', () {
    final geometricAlt = Altitude.fromMetres(3000.0);
    expect(isa.getGeopotentialAt(geometricAlt).m.roundToDouble(), 2999);
    expect(isa.getTemperatureAt(geometricAlt).kelvin, 268.65919845164115);
    expect(isa.getTemperatureAt(geometricAlt).celsius, -4.490801548358853);
    expect(isa.getPressureAt(geometricAlt).hPa, 701.2114441303628);
    expect(isa.getDensityAt(geometricAlt).kgMCub, 0.9092543375732786);
    expect(isa.getGravityAccelAt(geometricAlt).msSq, 9.797400285127708);
    expect(isa.getSoundSpdAt(geometricAlt).ms, 328.58355557958123);
  });
  test('Standard Atmosphere at 10000 m', () {
    final geometricAlt = Altitude.fromMetres(10000.0);
    expect(isa.getGeopotentialAt(geometricAlt).m.roundToDouble(), 9984);
    expect(isa.getTemperatureAt(geometricAlt).kelvin, 223.25209264797857);
    expect(isa.getTemperatureAt(geometricAlt).celsius, -49.897907352021406);
    expect(isa.getPressureAt(geometricAlt).hPa, 264.99873597786956);
    expect(isa.getDensityAt(geometricAlt).kgMCub, 0.4135103314780863);
    expect(isa.getGravityAccelAt(geometricAlt).msSq, 9.775868442887434);
    expect(isa.getSoundSpdAt(geometricAlt).ms, 299.5316622635759);
  });
  test('Standard Atmosphere at 50000 m', () {
    final geometricAlt = Altitude.fromMetres(80000.0);
    expect(isa.getGeopotentialAt(geometricAlt).m.roundToDouble(), 79006);
    expect(isa.getTemperatureAt(geometricAlt).kelvin, 198.63857625086882);
    expect(isa.getTemperatureAt(geometricAlt).celsius, -74.51142374913115);
    expect(isa.getPressureAt(geometricAlt).hPa, 0.010524652058322474);
    expect(isa.getDensityAt(geometricAlt).kgMCub, 0.0000184578985307727);
    expect(isa.getGravityAccelAt(geometricAlt).msSq, 9.56439894343608);
    expect(isa.getSoundSpdAt(geometricAlt).ms, 282.5379334435868);
  });
}
