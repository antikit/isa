## 1.0.0

* First version released.
* Dependency on measures package updated.


## 0.0.4

* Project files reorganized.

## 0.0.3

* Minor changes and fixes.

## 0.0.2

* Some API documentation provided.

## 0.0.1

* Initial version commit.
